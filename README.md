# Web performance optimization

This module aims to provide advanced settings
for frontend performance enhancements:

- DNS Prefetch: tells the browser which domains you'll be using
