<?php
/**
 * @file
 *   Administrative include for webperf.module
 */

function webperf_prefetch_settings() {
  $form = array();

  $form['prefetch'] = array(
    '#type' => 'fieldset',
    '#collapsed' => 'collapsible',
    '#title' => 'Prefetch settings',
  );

  // Implement DNS prefetching on supporting browsers
  $form['prefetch']['webperf_prefetch_dnsprefetch'] = array(
    '#type' => 'textfield',
    '#title' => 'DNS prefetch domain',
    '#description' => t('Specify a domain to appear as a &lt;link> tag with <code>rel="dns-prefetch"</code>. !readmore.', array(
      '!readmore' => l(
        t('Read more'),
        'http://www.igvita.com/posa/high-performance-networking-in-google-chrome/#dns-prefetching',
        array('attributes' => array('target' => '_blank'))
      ),
    )),
    '#default_value' => variable_get('webperf_prefetch_dnsprefetch', WEBPERF_DNSPREFETCH_DEFAULT),
  );

  return system_settings_form($form);
}
